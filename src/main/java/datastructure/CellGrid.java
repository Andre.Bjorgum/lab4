package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState[][] grid;


    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
		this.columns = columns;
		this.grid = new CellState[rows][columns];

		for (int row = 0; row < rows; row++){
		    for (int column = 0; column < columns; column++){
		        grid[row][column] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || column < 0 || row >= rows || column >= columns){
            throw new IndexOutOfBoundsException("Invalid call. ");
        }
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {

        /**
        if (row < 0 || row >= numRows()){
            throw new IndexOutOfBoundsException("Row exception!");
        }
        else if (column < 0 || column >= numColumns()){
            throw new IndexOutOfBoundsException("Column exeption!");
        }
         */

        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid copy = new CellGrid(rows, columns, null);

        for (int row = 0; row < rows; row ++){
            for(int column = 0; column < columns; column++){
                CellState c = get(row, column);
                copy.set(row, column, c);
            }
        }

        return copy;
    }
    
}
